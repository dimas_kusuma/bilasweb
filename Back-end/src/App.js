import React, { Component } from 'react';
import {Route, Switch, BrowserRouter} from 'react-router-dom';
import Login from './dashboard/SignIn';
import Dashboard from './dashboard/Dashboard';

class App extends Component {
  render () {
    return (
      <BrowserRouter>
        <div>
          <Switch>
       <Route exact path='/' component={Dashboard} />
       <Route path='/login' component={Login} />
       </Switch>
     </div>
     </BrowserRouter>
   );
 }
}
export default App;