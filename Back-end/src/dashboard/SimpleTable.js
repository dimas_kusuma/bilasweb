import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import {db} from '../firebase';
import * as moment from 'moment';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

class Tabel extends React.Component { 
  constructor(props){
    super(props);
    this.state={
        suggestions:null,
        keyid: null,
        coba:[],
        coba2:[] 
     };
    
  }
  
  
  componentDidMount() {
    moment.updateLocale('en', {
      weekdays : [
          "Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"
      ],
      monthsShort : [
        "Januari", "Februari", "Maret", "April", "Mei", "Juni",
        "Juli", "Agustus", "September", "Oktober", "November", "Desember"
    ]
  });
  this.onLoad();
  }  
  
  onLoad() {
  db.collection('outlet').onSnapshot((querySnapshot) => {
  const todos = [];
  querySnapshot.forEach((doc) => {
    const { kota, aktif_sampai, uid  } = doc.data();
   const namaotlet = doc.data().nama
    db.collection('user').where('level', "==", "pemilik").where('uid', "==", uid).onSnapshot((querySnapshot) => {
      
      querySnapshot.forEach((doc) => {
        const { nama  } = doc.data();
        todos.push({
          key:doc.id,
          kota,
          nama,
          namaotlet,
          aktif_sampai:aktif_sampai.seconds,
        });
      })
      this.setState({coba:todos});
      })
  })
  })
  }
  
  render(){

  return (
    <Paper>
      <Table>
        <TableHead>
          <TableRow>
            <CustomTableCell>Nama</CustomTableCell>
            <CustomTableCell align="left">Lokasi</CustomTableCell>
            <CustomTableCell align="left">Aktif Sampai</CustomTableCell>
            <CustomTableCell align="left">Pemilik</CustomTableCell>

          </TableRow>
        </TableHead>
        <TableBody>
        {this.state.coba && this.state.coba.map((todos, index) =>
              <TableRow key={index}>
                <TableCell component="th" scope="row">
                  {todos.namaotlet}
                </TableCell>
                <TableCell align="left">{todos.kota}</TableCell>
                <TableCell align="left">{moment(todos.aktif_sampai*1000).format("DD MMM YYYY")}</TableCell>
                <TableCell align="left">{todos.nama}</TableCell>
              </TableRow>
            
          )}
        
        </TableBody>
      </Table>
    </Paper>
  )
        }
      }
     

  
export default Tabel ;