import React from 'react';
import ResponsiveContainer from 'recharts/lib/component/ResponsiveContainer';
import LineChart from 'recharts/lib/chart/LineChart';
import Line from 'recharts/lib/cartesian/Line';
import XAxis from 'recharts/lib/cartesian/XAxis';
import YAxis from 'recharts/lib/cartesian/YAxis';
import CartesianGrid from 'recharts/lib/cartesian/CartesianGrid';
import Tooltip from 'recharts/lib/component/Tooltip';
import Legend from 'recharts/lib/component/Legend';

const data = [
  { name: 'Jan', Visits: 2200, Orders: 3400 },
  { name: 'Feb', Visits: 1280, Orders: 2398 },
  { name: 'Mar', Visits: 5000, Orders: 4300 },
  { name: 'Apr', Visits: 4780, Orders: 2908 },
  { name: 'Mei', Visits: 5890, Orders: 4800 },
  { name: 'Jun', Visits: 4390, Orders: 3800 },
  { name: 'Jul', Visits: 4490, Orders: 4300 },
  { name: 'Agt', Visits: 1280, Orders: 2398 },
  { name: 'Sep', Visits: 5000, Orders: 4300 },
  { name: 'Okt', Visits: 4780, Orders: 2908 },
  { name: 'Nov', Visits: 5890, Orders: 4800 },
  { name: 'Des', Visits: 4390, Orders: 3800 },
];

function SimpleLineChart() {
  return (
    // 99% per https://github.com/recharts/recharts/issues/172
    <ResponsiveContainer width="99%" height={320}>
      <LineChart data={data}>
        <XAxis dataKey="name" />
        <YAxis />
        <CartesianGrid vertical={false} strokeDasharray="3 3" />
        <Tooltip />
        <Legend />
        <Line type="monotone" dataKey="Visits" stroke="#82ca9d" />
        <Line type="monotone" dataKey="Orders" stroke="#8884d8" activeDot={{ r: 8 }} />
      </LineChart>
    </ResponsiveContainer>
  );
}

export default SimpleLineChart;