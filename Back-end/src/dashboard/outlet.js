import React from 'react';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import {db} from '../firebase';
import Card from '@material-ui/core/Card';
class Outlet extends React.Component { 

constructor(props){
  super(props);
  this.state={
      suggestions:null,
      keyid: null,
      coba:null,
      coba2:null,
      todos:null       
   };
  
}


componentDidMount() {
this.onLoad();
}  

onLoad() {
db.collection('outlet').onSnapshot((querySnapshot) => {
const todos = [];
querySnapshot.forEach((doc) => {
  const { alamat  } = doc.data();
  todos.push({
    key:doc.id,
                  // DocumentSnapshot
                  alamat 
  });
this.setState({coba:todos.length});
//  console.log("cek gan = "+todos.length);
})
})
db.collection('user').onSnapshot((querySnapshot) => {
  const todos = [];
  querySnapshot.forEach((doc) => {
    const { level  } = doc.data();
    todos.push({
      key:doc.id,
                    // DocumentSnapshot
                    level 
    });
  this.setState({coba2:todos.length});
  //  console.log("cek gan = "+todos.length);
  })
  })
}

render(){
  return (
    <div>
      <Grid container spacing={24}>
        <Grid item xs={6}>
        <Card>
        <CardContent>
        <Typography variant="h5" component="h2">
        {this.state.coba}
        </Typography><br/>
          <Typography component="p">
          Outlet Bilas
        </Typography>
      </CardContent>
        </Card>
</Grid>
        <Grid item xs={6}>
        <Card>
        <CardContent>
        <Typography variant="h5" component="h2">
        {this.state.coba2}
        </Typography><br/>
          <Typography component="p">
          Pemilik
        </Typography>
      </CardContent>
        </Card>
        </Grid>
      </Grid>
    </div>
  );
}
}
// CenteredGrid.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

export default Outlet;