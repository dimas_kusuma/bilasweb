import * as firebase from 'firebase';

var config = {
  apiKey: "AIzaSyB1CXb3_whLLgAA_vFd-89P67D_8rCqkPU",
  authDomain: "laundry-ec3fe.firebaseapp.com",
  databaseURL: "https://laundry-ec3fe.firebaseio.com",
  projectId: "laundry-ec3fe",
  storageBucket: "laundry-ec3fe.appspot.com",
  messagingSenderId: "631279940888"
};
firebase.initializeApp(config);

export const db = firebase.firestore();
const settings = {timestampsInSnapshots: true};
db.settings(settings);
export default firebase;